#while ! pg_isready -h unb_games_database -p 5432 -q -U postgres; do
#  >&2 echo "Waiting for database service start properly!"
#	sleep 1
#done

#>&2 echo "Postgres is up - continuing..."

cd /app/frontend/
timeout 140s npm run webpack:dev

cd /app/backend/
python3 manage.py makemigrations game information media
python3 manage.py migrate
python3 manage.py collectstatic

./gunicorn_start.sh
