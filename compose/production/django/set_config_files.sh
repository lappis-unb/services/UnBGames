UNB_GAMES_PATH="/app"

#Set Gunicorn configuration file
mv $UNB_GAMES_PATH/compose/production/django/gunicorn_start.sh $UNB_GAMES_PATH/backend

#Update settings.py file
rm $UNB_GAMES_PATH/backend/core/settings.py
mv $UNB_GAMES_PATH/compose/production/django/settings.py $UNB_GAMES_PATH/backend/core

#Update wsgi.py file
rm $UNB_GAMES_PATH/backend/core/wsgi.py
mv $UNB_GAMES_PATH/compose/production/django/wsgi.py $UNB_GAMES_PATH/backend/core

#Update requirements.txt file
rm $UNB_GAMES_PATH/backend/requirements.txt
mv $UNB_GAMES_PATH/compose/production/django/requirements.txt $UNB_GAMES_PATH/backend/

mv $UNB_GAMES_PATH/compose/production/django/start_prod.sh $UNB_GAMES_PATH/frontend/

